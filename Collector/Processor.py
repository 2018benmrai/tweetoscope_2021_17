from kafka import KafkaConsumer
import logger
import json
import TweetoscopeCollectorParams

"""
NB : tweets are dictionnaries : 

tweet : {"type" : "tweet"|"retweet",
    //                "tweet_id" : 40556, 
    //                "msg": "...", 
    //                "time": 42336,
    //                "magnitude": 1085.0,
    //                "source": 0,
    //                "info": "blabla"}
"""

"""
Class Processor:
Parameters:
:param id_source : it is the key of our processor here
- dict_cascades : keys = cascade_id
                  values = list(tweets_assciated_to_cascade)
- queue_cascades : queue used to class the cascade_id's from the "oldest" one /
                    (last cascade in the processor's cascades that didn't receive message) /
                    to the newest one

- processor_time : it is the current clock of our processor. We update it before looking
                   after cascade series and cascade properties, in order to have the real
                   updated times for our tests
- observations : parameters of time window (see collector.ini). Ex : [600,1200]
- terminated : parameter, condition of end of cascade (see collector.ini)
- log : attribute to send log messages
- windows_dict : dictionnary which keys are observations, and values are list of cascade_ids. 
                 They permit the treatment of cascades through observation windows


"""

class Processor:
    def __init__(self, id_source, observations: list, terminated):
        """
        Builder of Processor.

        :param id_source:
        :param observations:
        :param terminated:

        """
        self.id_source = id_source
        self.dict_cascades = {}
        self.queue_cascades = []
        self.processor_time = 0
        self.observations = observations
        self.terminated = terminated
        self.log = logger.get_logger("processor " + str(self.id_source), broker_list=TweetoscopeCollectorParams.brokers, debug=False)
        self.windows_dict = {}

        for i in range (len(observations)) :
            self.windows_dict[self.observations[i]] = []

    def update_clock(self, tweet: dict):
        """
        Update processor time with new tweet's time

        :param tweet:
        :return: None

        """
        self.processor_time = int(tweet["t"])

    def append_tweet(self, tweet: dict):
        """
        - If we have a tweet :
            - If we have a tweet we create a new cascade in the dictionnary
            - We update the queue_cascade and append the new cascade_id to the first observation window's list in windows_dict.

        :param tweet:
        :return:
        """
        if tweet["type"] == "tweet":
            self.log.info("La cascade correspondant à l'ID " + str(tweet["tweet_id"]) + " a été créée au temps " + str(self.processor_time))
            """
            We initiate the new cascade in the dictionnary
            We update the queue_cascade and the list_window_cascades
            """

            self.dict_cascades[tweet["tweet_id"]] = [tweet]
            self.queue_cascades.append(tweet["tweet_id"])
            self.windows_dict[self.observations[0]].append(tweet["tweet_id"])
        else:
            if tweet["tweet_id"] in self.dict_cascades.keys():
                self.dict_cascades[tweet["tweet_id"]].append(tweet)
                #We put the cascade_id at the end of the queue, since it is associated to the most recent tweet
                self.queue_cascades.remove(tweet["tweet_id"])
                self.queue_cascades.append(tweet["tweet_id"])

                #self.list_window_cascades.insert(0, tweet["tweet_id"])
            else:
                self.log.info("The tweet " + str(tweet["tweet_id"]) + " is a retweet from a dead cascade")
                pass

    def is_cascade_dead(self, cascade_id):
        """

        :param cascade_id:
        :return:
        """
        tweets_cascade = self.dict_cascades[cascade_id]
        last_tweet = tweets_cascade[-1]
        if self.processor_time - int(last_tweet["t"]) > self.terminated:
            return True
        return False

    def pop_finished_cascades(self):
        """

        :return:
        """
        res = []
        list_indices = []
        def create_output_cascade(cascade_id):
            tweets_cascade = self.dict_cascades[cascade_id]
            last_tweet = tweets_cascade[-1]
            res = {}
            res["type"] = "size"
            res["cid"] = cascade_id
            res["n_tot"] = len(self.dict_cascades[cascade_id])
            res["t_end"] = int(last_tweet["t"])
            return res

        for cascade_id in self.queue_cascades:
            if self.is_cascade_dead(cascade_id):
                res.append(create_output_cascade(cascade_id))
                list_indices.append(cascade_id)
                tweets_cascade = self.dict_cascades[cascade_id]
                last_tweet = tweets_cascade[-1]
                self.log.debug("La cascade : " + str(cascade_id) +
                             " va être éliminée, son dernier tweet date du " + str(last_tweet["t"]) +
                             " et le temps actuel est " + str(self.processor_time ))

            else :
                break
        return res,list_indices
        
    def remove_dead_cascades(self, list_indices) :
        """

        :param list_indices:
        :return:
        """
        for cascade_id in list_indices:
            self.dict_cascades.pop(cascade_id)
            self.queue_cascades.remove(cascade_id)
            for time_window in self.observations :
                if cascade_id in self.windows_dict[time_window]:
                    self.windows_dict[time_window].remove(cascade_id)
                

                #TODO : determine how to send information of the cascade to topic cascade_properties


    def window_cascade_ready(self, cascade_id, window_param_time):
        """

        :param cascade_id:
        :param window_param_time:
        :return:
        """
        cascade_tweets = self.dict_cascades[cascade_id]
        first_tweet = cascade_tweets[0]
        if self.processor_time - int(first_tweet["t"]) > int(window_param_time):
            return True
        return False

    def pop_window_cascades(self):
        """

        :return:
        """
        cascade_series = []

        def create_partial_output_cascade(cascade_id, time_window):
            """

            :param cascade_id:
            :param time_window:
            :return:
            """
            res ={}
            cascade_times = self.dict_cascades[cascade_id]
            res["type"] = "serie"
            res["cid"] = cascade_id
            res["tweets"] = [(cascade_times[i]["t"], cascade_times[i]["m"]) for i in range(len(cascade_times))]
            res["T_obs"] = time_window
            res["msg"] = "blah blah"
            return res

        for time_window_index in range(len(self.observations)):
            list_of_cascade_of_window = self.windows_dict[self.observations[time_window_index]]
            if time_window_index == len(self.observations) -1:
                for cascade_id in list_of_cascade_of_window:
                    if self.window_cascade_ready(cascade_id, self.observations[time_window_index]):
                        cascade_series.append(create_partial_output_cascade(cascade_id, self.observations[time_window_index]))
                        self.windows_dict[self.observations[-1]].remove(cascade_id)
                    else:
                        break
            else:
                for cascade_id in list_of_cascade_of_window:
                    if self.window_cascade_ready(cascade_id, self.observations[time_window_index]):
                        cascade_series.append(create_partial_output_cascade(cascade_id, self.observations[time_window_index]))
                        self.windows_dict[self.observations[time_window_index]].remove(cascade_id)
                        self.windows_dict[self.observations[time_window_index +1]].append(cascade_id)
                    else:
                        break
        for cascade in cascade_series :
            cascade_tweets = self.dict_cascades[cascade["cid"]]
            first_tweet = cascade_tweets[0]
            t = str(first_tweet["t"]) 
            self.log.debug("La cascade " + str(cascade) + 
                           " a une durée de " + str(self.processor_time) + " - " + 
                           t + " = " + 
                           str(self.processor_time - int(t))) 
        return cascade_series

    #TODO:determine how to send information of the cascade to topic cascade_series


