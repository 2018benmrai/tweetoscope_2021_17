#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 24 18:03:19 2021

@author: hadrich
"""


observation = []
with open ("collector.ini",'r') as f :
    for line in f :
        line.strip()
        if line[0] == '#' or line[0] == '[' :
            None
        else :
            params = line.strip().split('=')
            if params [0] == "brokers" : 
                continue
            if params [0] == "in" : 
                topicIn = params [1]
            if params [0] == "out_series" : 
                out_series = params [1]
            if params [0] == "out_properties" : 
                out_properties = params [1]
            if params [0] == "observation" : 
                observation.append(params [1])
            if params [0] == "terminated" : 
                terminated = params [1]
            if params [0] == "min_cascade_size" : 
                min_cascade_size = params [1]

with open ("local.ini", 'r') as f :
    for line in f :
        line.strip()
        if line[0] == '#' or line[0] == '[' :
            None
        else :
            params = line.strip().split('=')
        if params [0] == "run_local" : 
            if params[1] == "True":
                brokers = "localhost:9092"
            else:
                brokers = "kafka-service:9092"


#Test récupération paramètres
print (brokers)
print (topicIn)
print (out_series)
print (out_properties)
print (terminated)
print (observation)
print (min_cascade_size)

