import TweetoscopeCollectorParams
import json                       # To parse and dump JSON
from kafka import KafkaConsumer   # Import Kafka consumer

consumer_dead_cascades = KafkaConsumer(TweetoscopeCollectorParams.out_properties,
                                bootstrap_servers = TweetoscopeCollectorParams.brokers,
                                value_deserializer=lambda v: json.loads(v.decode('utf-8')),
                                key_deserializer= lambda v: v.decode())

for msg in consumer_dead_cascades:
    print((f"msg: ({msg.key}, {msg.value})"))