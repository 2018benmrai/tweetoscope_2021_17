#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 24 18:03:19 2021

@author: hadrich
"""
import TweetoscopeCollectorParams
import json                       # To parse and dump JSON
from kafka import KafkaConsumer   # Import Kafka consumer
from kafka import KafkaProducer
import Processor

"""
Consumer used to listen on topicIn
"""


consumer_tweets = KafkaConsumer(TweetoscopeCollectorParams.topicIn, bootstrap_servers = TweetoscopeCollectorParams.brokers,value_deserializer=lambda v: json.loads(v.decode('utf-8')),key_deserializer= lambda v: v.decode())
producer = KafkaProducer(bootstrap_servers = TweetoscopeCollectorParams.brokers, value_serializer= lambda v: json.dumps(v).encode("utf-8"), key_serializer= str.encode)
dict_of_processors ={}

"""

MAIN : 
input : msg tweets --> {"type" : "tweet"|"retweet",
    //                "tweet_id" : 40556, 
    //                "msg": "...", 
    //                "time": 42336,
    //                "magnitude": 1085.0,
    //                "source": 0,
    //                "info": "blabla"}
    
- return : sending cascade series to topic --> {"type" : "serie", 
    //                "cid": "...", 
    //                "tweets": 42336,
    //                "T_obs": 1085.0,
    //                "msg": "blah blah"}
    
            sending cascade properties to topic --> {"type" : "size", 
    //                "cid": 3083, 
    //                "n_tot": 1203,
    //                "t_end": 48632 }
            
"""
for msg in consumer_tweets:
    key = int(msg.key)
    # Blocking call waiting for a new message
    if key in list(dict_of_processors.keys()):
        """
        We implement a class Processor in order to handle each source with a processor .
        1/ If we have a tweet msg with a new source_id (see below l 77) :
            - Create a new instance of a Processor
            - Updating clock to current time
            - Append tweet to the processor
        2/ Elif we have a message with a known source_id:
            - We use the instance of processor associated to the source_id
            - We search in our processor the dead cascades and the cascade series to pop
            - We send the cascades on topic
            - We update our processor
            
        """
        processor = dict_of_processors[key]
        processor.update_clock(msg.value)
        finished_cascades, list_indices = processor.pop_finished_cascades()
        if finished_cascades:
            for cascade in finished_cascades:
                for time_window in TweetoscopeCollectorParams.observation:
                    producer.send(TweetoscopeCollectorParams.out_properties, key = time_window, value = cascade)
                    processor.log.info(" DEAD CASCADE : ( " + str(cascade["cid"]) + " , n_tot : " + str(cascade["n_tot"])
                                       + " , last_tweet_t : " + str(cascade["t_end"]) + " , current time : " +
                                       str(processor.processor_time) + str(" )")

                                        )
        cascade_series = processor.pop_window_cascades()
        if cascade_series:
            for cascade in cascade_series:
                producer.send(TweetoscopeCollectorParams.out_series, key = "", value = cascade)
                tweets_timestamps = [j[0] for j in cascade["tweets"]]
                processor.log.info("CASCADE SERIE : ( cascade_id " + str(cascade["cid"]) + " , T_obs : " +
                                   str(cascade["T_obs"]) + " , first_tweet_t" + str(tweets_timestamps[0])
                                   + " , last_tweet: " + str(tweets_timestamps[-1]) + " , current time : "
                                   + str(processor.processor_time) + " )")
        processor.append_tweet(msg.value)
        processor.remove_dead_cascades(list_indices)

    else:
        processor = Processor.Processor(key, TweetoscopeCollectorParams.observation, int(TweetoscopeCollectorParams.terminated))
        dict_of_processors[key] = processor
        processor.update_clock(msg.value)
        processor.append_tweet(msg.value)





