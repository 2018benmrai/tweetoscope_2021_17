# Tweetoscope_2021_17

![image](https://drive.google.com/uc?export=view&id=1ImJ9-gvKyRiiCmmvv83aMM3XaUIeoFne)



### Objectives of the project 
---

This project consists in predicting the popularity of tweets. by popularity we mean the number of people that will retweet an original tweet.
We don't collect real tweets but instead we use a generator of tweets that simulates tweets and retweets. Each one is associated with a time of emission and a magnitude corresponding to the number of followers of the isuer of the tweet/retweet.

You can find the complete description of the project [here](https://sdi.metz.centralesupelec.fr/spip.php?article25).


#### The architecture of our application 
---
In order to make a prediction, our application is based on the following stages :
- The ***Tweet Generator*** (written in C++) generates tweets 
- Then the ***Tweet Colector*** (in python as for the rest of the project) collects tweets from the generator. It organize them in partial cascades and send them to the estimator at different time windows. When a cascade is over, it send its final information to the predictor for it to compare the true size of a cascade with its prediction.
- The ***Hawkes Estimator*** receives the partial cascades, makes an estimation of the final size and parameters of the cascade. It sends them to the predictor.
- The ***Predictor*** receives the estimated popularities of a cascade from the Hawkes Estimator and its real final size from the Colector. Thanks to a random forest model, it ajusts the prediction of the estimator and send it to an alerts topic. It then sends the prediction error on a stats topic, and a new sample on a samples topic to train the random forest model. There is on random forest model for every time window.

Here is the architecture of the application :
![image](https://drive.google.com/uc?export=view&id=1YHOqcb76-i7xukSmC3EjaO7y2dfqAs4B)

#### How to launch the application

### In local:

`export KAFKA_PATH=~/kafka_2.13_2.4.1/`   *Set the environment variable's path.*

`./kafka.sh start --zooconfig $KAFKA_PATH/config/zookeeper.properties --serverconfig $KAFKA_PATH/config/server.properties`   *Starting zookeeper and kafka servers.*

_Run the following commands on several linux terminals and in the good directories each time._

_1- :_  `python3 TweetoscopeCollector.py`    

_2- :_  `python3 estimator.py`

_3- :_  `python3 Predictor.py`

_4- :_  `python3 Learner.py`

_5- :_  `g++ -o tweet-generator -03 -Wall -std=c++17 tweet-generator.cpp pkg-config --libs --cflags gaml cppkafka -lpthread`     *Compiling tweet-generator code.*

_5- :_  `./tweet-generator params.config`    *Execute file tweet-generator.*

### Deployed with Minikube

Do not forget to install Minikube !!!

Download the two yml files `server.yml` and `tweetoscope-launcher.yml`.

Then, run the following commands on a nutshell:

`minikube start`     *start minikube*

_run the following commands from the directory where you downloaded the two yml files._

`kubectl apply -f server.yml`    *start the zookeeper and the broker.*

`kubectl apply -f tweetoscope-launcher.yml`   *deploy tweetoscope*

`kubectl get pods`     *provide the list of the pods which corresponds to the different services of our application (logger, predictor, etc...)*

`kubectl logs <podname>`    *get the logs of podname*

`kubectl logs logger-pod`   *get the logs of the logger, ie all the logs*


### Videos

Here you can find the two videos presenting how the pipeline works : 1) in local 2) with the deployed version :

https://drive.google.com/drive/folders/10v_2izW7fulEtg8DqwbJNWLihzeTDPXo?usp=sharing

#### Members of the project : 
---
- Khalil Hadrich
- Idriss Ben M'rad
- Oscar Ambrois
- Hugo Prudent









