import json  # To parse and dump JSON
from kafka import KafkaConsumer  # Import Kafka consumer
from kafka import KafkaProducer  # Import Kafka producer
import LearnerParams
import pandas as pd
import time
from sklearn.ensemble import RandomForestRegressor
import pickle

# Producer on the models topic:
producer = KafkaProducer(bootstrap_servers=LearnerParams.brokers,
                         key_serializer=str.encode)


# Consumer of the the samples topic :
consumer_samples = KafkaConsumer(LearnerParams.topicIn,
                                 bootstrap_servers=LearnerParams.brokers,
                                 value_deserializer=lambda v: json.loads(v.decode('utf-8')),
                                 key_deserializer=lambda v: v.decode())


# We store in a dictionnary the different dataframes of every time windows
dataframes = {}
for window in LearnerParams.window_times:
    dataframes[int(window)] = pd.DataFrame(columns=['beta', 'n_etoile', 'G', 'w'])


def update_models():
    """
    send in the models topic the new models trained on every time windows
    """

    for window in LearnerParams.window_times:
        if len(dataframes[int(window)]) > 10 :
            rf = RandomForestRegressor()
            X = dataframes[int(window)].drop('w', axis=1)
            y = dataframes[int(window)]['w']
            rf.fit(X.values, y.values)
            rf_pickled = pickle.dumps(rf)
            producer.send(LearnerParams.out_models, key=window, value=rf_pickled)


# Keep the curent time to call update_models every 5s (too low value but better for the demo)
now = time.time()

# for each sample received, we add it to the dataframe of its time window
for msg in consumer_samples:
    window = int(msg.key)
    beta, n_etoile, G = msg.value['X']
    w = msg.value['W']
    dataframes[window] = dataframes[window].append({'beta': beta, 'n_etoile': n_etoile, 'G': G, 'w': w}, ignore_index=True)

    # we update the models and send them every 5s
    if (time.time() - now) > 5:
        update_models()
        print(time.time())
        now = time.time()
