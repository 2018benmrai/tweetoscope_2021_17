from kafka import KafkaConsumer
import PredictorParams
import pickle

consumer_out_models = KafkaConsumer(PredictorParams.topicIn_models,
                                bootstrap_servers = PredictorParams.brokers,
                                value_deserializer=lambda v: pickle.loads(v),
                                key_deserializer= lambda v: v.decode())
for msg in consumer_out_models:
    print(f"msg: ({msg.key}, {msg.value})")