import pytest
import pandas as pd
import numpy as np
from estimator.py import loglikelihood, compute_MAP, prediction, prediction_from_MAP

@pytest.fixture
def history():
    '''Returns the history '''
    return np.array([[1,2],[3,2],[4,2]])

@pytest.fixture
def params():
    '''Returns (p,beta)'''
    return (0.5,4)

@pytest.fixture
def t():
    '''Returns the current time'''
    return 6



def test_loglikelihood(params,history,t) :
    assert type(loglikelihood(params, history, t))==float
    

    
def test_compute_MAP(history, t, prior_params = [ 0.02, 0.0002, 0.01, 0.001, -0.1], max_n_star = 1, display=False) :
    assert type(compute_MAP(params, history, t))==float
