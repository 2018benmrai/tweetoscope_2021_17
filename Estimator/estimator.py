# -*- coding: utf-8 -*-
"""
Created on Wed Dec  1 17:03:10 2021

@author: Oscar
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 22:01:25 2021

@author: Oscar
"""

import json 
import numpy as np                # To parse and dump JSON
from kafka import KafkaConsumer   # Import Kafka consumer
from kafka import KafkaProducer   # Import Kafka producer
import estimator_functions
import estimator_params
import logger

#get the logger
logger = logger.get_logger('estimator', broker_list=estimator_params.brokers, debug=True)  

# Producers :
producer = KafkaProducer(bootstrap_servers = estimator_params.brokers, 
                         value_serializer= lambda v: json.dumps(v).encode("utf-8"), 
                         key_serializer= str.encode)



# Reading messages from the topic cascade_series :
consumer_cascade_series = KafkaConsumer(estimator_params.topicIn_cascade_series, 
                                bootstrap_servers = estimator_params.brokers,
                                value_deserializer=lambda v: json.loads(v.decode('utf-8')),
                                key_deserializer= lambda v: v.decode())


# Blocking call waiting for a new message
for msg in consumer_cascade_series:         
    msg_value = msg.value
    #calling prediction_from_MAP on cascade msg_value['tweets'] :
    n_obs, n_supp, params = estimator_functions.prediction_from_MAP(np.array(msg_value['tweets'])) 
    
    msg_out = {
    'type':'parameters',
    'cid':msg_value['cid'],
    'msg':msg_value['msg'],
    'n_obs':n_obs,
    'n_supp':n_supp,
    'params':params
    }
    
    #sending msg_out to topic cascade_properties.
    producer.send(estimator_params.topicOut_cascade_properties, key=msg_value['T_obs'], value=msg_out) 

    #sending infos about the cascade that has just been sent to the topic cascade_properties.
    logger.info("The predicted final size of the cascade " + str(msg_value['cid']) + " is " + str(n_obs + n_supp) + " and its estimated parameters are p=" + str(params[0]) + " and beta=" + str(params[1]))

    #checking weird values for p and n_star (debug).
    if params[0] < 0 or params[0] > 1:
        logger.debug("estimated value of p is not between 0 and 1")
    
    if params[2] > 1:
        logger.debug("n_star is greator than 1")
        
