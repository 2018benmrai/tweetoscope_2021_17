# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 22:26:10 2021

@author: Oscar
"""

with open ("estimator.ini",'r') as f :
    for line in f :
        line.strip()
        if line[0] == '#' or line[0] == '[':
            None
        else :
            params = line.strip().split('=')
            if params[0] == "brokers" :
                continue
            if params[0] == "in":
                topicIn_cascade_series = params [1]
            if params[0] == "out":
                topicOut_cascade_properties = params[1]

with open ("local.ini", 'r') as f :
    for line in f :
        line.strip()
        if line[0] == '#' or line[0] == '[' :
            None
        else :
            params = line.strip().split('=')
            if params [0] == "run_local" : 
                if params[1]=="True":
                	brokers = "localhost:9092"
                else:
                    brokers = "kafka-service:9092"
#Test récupération paramètres
print (brokers)
print (topicIn_cascade_series)
print (topicOut_cascade_properties)

