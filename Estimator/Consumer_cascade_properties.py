# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 22:01:25 2021

@author: Oscar
"""
import json
from kafka import KafkaConsumer   # Import Kafka consumer
import estimator_params


consumer_cascade_properties = KafkaConsumer(estimator_params.topicOut_cascade_properties, 
                                bootstrap_servers = estimator_params.brokers,
                                value_deserializer=lambda v: json.loads(v.decode('utf-8')),
                                key_deserializer= lambda v: v.decode())


for msg in consumer_cascade_properties:
	print (f"msg: ({msg.key}, {msg.value})") 










