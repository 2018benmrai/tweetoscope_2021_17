import json                       # To parse and dump JSON
from kafka import KafkaConsumer   # Import Kafka consumer
from kafka import KafkaProducer   # Import Kafka producer
import PredictorParams
import numpy as np
import logger

logger = logger.get_logger('Predictor', broker_list=PredictorParams.brokers, debug=True) # to print logs


"""

Useful functions

"""

def msg_is_parameters(msg_value):
    """
    return True if the message type is "parameters"
    """
    return (msg_value['type'] == 'parameters')

def msg_is_size(msg_value):
    """
    return True if the message type is "size"
    """
    return (msg_value['type'] == 'size')


def prediction_from_rf(parameters, model):
    """
    return the prediction of n_tot and w from a sklearn model"

    parameters   -- must be a parameters message
    model        -- a sklearn model
    """

    p, beta, n_etoile, G = parameters['params']

    w = float(model.predict([[beta, n_etoile, G]]))
    n_tot = int(parameters['n_obs'] + w * G / (1 - n_etoile))

    return ((n_tot, w))

def save_prediction_in_memory(window, parameters, n_tot, w):
    """
    save the prediction in order to find it when the corresponding size message arrives

    window       -- int corresponding to the window
    parameters   -- must be a parameters message
    n_tot        -- n_tot predicted value
    w            -- w predicted value
    """

    predictions_history[(window, parameters['cid'])] = {'n_obs': parameters['n_obs'],'n_tot': n_tot, 'w': w, 'beta': parameters["params"][1], 'n_etoile': parameters["params"][2], 'G': parameters["params"][3]}

def write_on_alerts_topic(window, parameters, n_tot):
    """
    send a alert message on the alerts topic

    window       -- int corresponding to the window
    parameters   -- must be a parameters message
    n_tot        -- n_tot predicted value
    """

    msg = {
        'type': 'alert',
        'cid': parameters['cid'],
        'msg': parameters['msg'],
        'T_obs': window,
        'n_tot': n_tot
    }
    producer.send(PredictorParams.out_alerts, key="", value=msg)
    logger.info("The prediction for the cascade n° " + str(parameters['cid']) + " predicted at a time window = " + str(window) +" is n_tot = " + str(n_tot))



def find_previous_prediction(window, cid):
    """
    find the corresponding prediction when a size message arrives

    window       -- int corresponding to the window of the message
    cid          -- string corresponding to the casccade id of the message
    """

    return (predictions_history[(window, cid)])

def write_on_stats_topic(window, message_size, previous_prediction):
    """
    send a stat message on the stats topic

    window                -- int corresponding to the window of the message
    message_size          -- must be a size message
    previous prediction   -- the previous prediction of the size message
    """

    n_tot = previous_prediction['n_tot']
    n_true = message_size['n_tot']
    ARE = abs(n_tot - n_true) / n_true

    msg = {
        'type': 'stat',
        'cid': message_size['cid'],
        'T_obs': window,
        'ARE': ARE,
        'n_true':n_true,
        'n_pred':n_tot
    }
    print(msg)
    producer.send(PredictorParams.out_stats, key="", value=msg)
    logger.info("The ARE for the cascade n° " + str(message_size['cid']) + " predicted at a time window = " + str(window) +" is ARE = " + str(ARE) + " | n_true = " + str(n_true) + " and n_pred = " + str(n_tot))


def write_on_samples_topic(window, message_size, previous_prediction):
    """
    send a sample message on the samples topic

    window                -- int corresponding to the window of the message
    message_size          -- must be a size message
    previous prediction   -- the previous prediction of the size message
    """

    n_true = message_size['n_tot']
    n_obs = previous_prediction['n_obs']
    n_etoile = previous_prediction['n_etoile']
    G = previous_prediction['G']

    # find the true value of w :
    w_true = (n_true-n_obs) * (1-n_etoile)/G 

    msg = {
        'type': 'sample',
        'cid': message_size['cid'],
        'X': [previous_prediction['beta'], previous_prediction['n_etoile'], previous_prediction['G']],
        'W': w_true
    }
    print
    producer.send(PredictorParams.out_samples, key=str(window), value=msg)
    logger.info("The new sample for the cascade n° " + str(message_size['cid']) + " is X = [beta, n_etoile, G] = " + str(X) + " and w_true = " + str(w_true))




def was_waiting_for_prediction(window,cid):
    """
    check if there was a size message, corresponding to this window and key, waiting for a prediction

    window       -- int corresponding to the window of the message
    cid          -- string corresponding to the casccade id of the message
    """

    if (window,cid) in waiting_for_prediction.keys() :
        return True 
    return (False)

def find_waiting_size_message(window, cid) :
    """
    find the size message that was waiting for a prediction 

    window       -- int corresponding to the window of the message
    cid          -- string corresponding to the casccade id of the message
    """

    msg = waiting_for_prediction[(window,cid)]
    return (msg)

def save_in_waiting_list(window,message_size):
    """
    save the size_message in order to find it when the corresponding parameters message has been predicted

    window            -- int corresponding to the window of the message
    message_size      -- must be a size message
    """

    waiting_for_prediction[(window,message_size["cid"])] = message_size

def already_predicted(window, cid):
    """
    check if there was a parameters message, corresponding to this window and key, that was already predictied 

    window       -- int corresponding to the window of the message
    cid          -- string corresponding to the casccade id of the message
    """
    if (window,cid) in predictions_history.keys():
        return (True)
    return (False)

def delete_size_and_prediction(window,cid):
    """
    delete parameters and size messages from their memory dictionnaries

    window       -- int corresponding to the window of the message
    cid          -- string corresponding to the casccade id of the message
    """

    predictions_history.pop((window,cid))
    waiting_for_prediction.pop((window,cid))

import pickle
def update_model(window, pickle_model):
    """
    Transform a pickle model into a sklearn model and then save it into the models dictionnary

    window          -- int corresponding to the window of the message
    pickle_model    -- a model with the pickle format
    """

    models[window] = pickle.loads(pickle_model)





"""

MAIN

"""

# Producer :
producer = KafkaProducer(bootstrap_servers = PredictorParams.brokers, 
                         value_serializer= lambda v: json.dumps(v).encode("utf-8"),
                         key_serializer= str.encode)

# Consumers :

# Reading messages from the topic cascade_properties :
consumer_cascade_properties = KafkaConsumer(PredictorParams.topicIn_cascade_properties, 
                                bootstrap_servers = PredictorParams.brokers,
                                value_deserializer=lambda v: json.loads(v.decode('utf-8')),
                                key_deserializer= lambda v: v.decode())

# Reading messages from the topic models :
consumer_models = KafkaConsumer(PredictorParams.topicIn_models, 
                                bootstrap_servers = PredictorParams.brokers,
                                key_deserializer= lambda v: v.decode())



from sklearn.ensemble import RandomForestRegressor
import pandas as pd

# Create a models dictionnary. Each key is a time window and the value assossiated is the coressponding prediction model of w
models={}
for i in PredictorParams.observation :
    # we initiate with dump models before the first messages from models value update with the real models
    rf = RandomForestRegressor()
    X = pd.DataFrame([[0.001,0.5,100],[0.001,0.5,100],[0.001,0.5,100],[0.001,0.5,100],[0.001,0.5,100],[0.001,0.5,100],[0.001,0.5,100],[0.001,0.5,100],[0.001,0.5,100],[0.001,0.5,100]])
    y = pd.DataFrame([1,1,1,1,1,1,1,1,1,1])
    rf.fit(X,y)
    models[int(i)] = rf


# to keep in memory the informations of the predicted cascades that are waiting for their size messages 
predictions_history={}

# to keep in memory the informations of the size messages that are waiting for their predictions to be computed 
waiting_for_prediction={}



# Listening to consumers :

while True:
    # We keep in msg_cascade_propreties the messages from cascade properties topic during 1000 ms
    msg_cascade_propreties = consumer_cascade_properties.poll(timeout_ms=1000)

    # We keep in msg_models the messages from models topic during 10 ms
    msg_models = consumer_models.poll(timeout_ms=10)

    # We treat each received messages in my_cascade_properties :
    for tp, messages in msg_cascade_propreties.items():
        for message in messages:

            window = int(message.key)
            msg_value = message.value

            # if the message is a parameters message, we predict the n_tot and write it on alerts topic 
            if msg_is_parameters(msg_value) :
                n_tot,w = prediction_from_rf(parameters=msg_value, model = models[window])
                save_prediction_in_memory(window=window, parameters=msg_value, n_tot=n_tot, w =w)
                write_on_alerts_topic(window=window, parameters=msg_value, n_tot=n_tot)

                # if their was a size message waiting for the prediction, we write the new sample on samples topic
                # and the ARE stat on stats topic
                # we then delete the information of the cascade kept in memory
                if was_waiting_for_prediction(window=window, cid=msg_value["cid"]):
                    message_size = find_waiting_size_message(window=window, cid=msg_value["cid"])
                    previous_prediction = find_previous_prediction(window=window, cid=msg_value['cid'])
                    write_on_stats_topic(window=window, message_size=message_size, previous_prediction=previous_prediction)
                    write_on_samples_topic(window=window, message_size=message_size, previous_prediction=previous_prediction)
                    delete_size_and_prediction(window=window,cid=msg_value["cid"])


            # if the message is a size message, we save it in size messages waiting list
            elif msg_is_size(msg_value) :
                save_in_waiting_list(window=window,message_size=msg_value)

                # if the cascade has already been predicted, we write the new sample on samples topic
                # and the ARE stat on stats topic
                # we then delete the information of the cascade kept in memory
                if already_predicted(window=window, cid=msg_value["cid"]) :
                    previous_prediction = find_previous_prediction(window=window, cid=msg_value['cid'])
                    write_on_stats_topic(window=window, message_size=msg_value, previous_prediction=previous_prediction)
                    write_on_samples_topic(window=window, message_size=msg_value, previous_prediction=previous_prediction)
                    delete_size_and_prediction(window=window,cid=msg_value["cid"])


    # We update the models with the new ones received in msg_models :
    for tp, messages in msg_models.items():
        for message in messages :
            window = int(message.key)
            msg_value = message.value
            update_model(window=window, pickle_model = msg_value)

         
