import pytest
from Predictor import msg_is_parameters,msg_is_size, prediction_from_rf, was_waiting_for_prediction, find_waiting_size_message, already_predicted, delete_size_and_prediction
import pandas as pd
from sklearn.ensemble import RandomForestRegressor

@pytest.fixture
def size_message():
    '''Returns a size_message instance '''
    return ({'type' : 'size', 'cid': 'tw23981', 'n_tot': 127, 't_end': 4329 })

@pytest.fixture
def parameters_message():
    '''Returns a parameters_message instance '''
    return ({ 'type': 'parameters', 'cid': 'tw23981', 'msg' : 'blah blah', 'n_obs': 32, 'n_supp' : 120, 'params': [ 0.0423, 124.312, 0.5, 20 ] })

@pytest.fixture
def window():
    '''Returns a window instance '''
    return 600

@pytest.fixture
def w():
    '''Returns a w instance '''
    return 0.1

@pytest.fixture
def n_tot():
    '''Returns a n_tot instance '''
    return 100

@pytest.fixture
def rf_model():
    '''Returns a random forest model instance '''
    rf = RandomForestRegressor()
    X = pd.DataFrame([[0.001,0.5,100],[0.001,0.5,100],[0.001,0.5,100],[0.001,0.5,100],[0.001,0.5,100],[0.001,0.5,100],[0.001,0.5,100],[0.001,0.5,100],[0.001,0.5,100],[0.001,0.5,100]])
    y = pd.DataFrame([1,1,1,1,1,1,1,1,1,1])
    rf.fit(X,y)
    return (rf)




def test_msg_is_parameters(size_message,parameters_message):
    assert msg_is_parameters(parameters_message) == True
    assert msg_is_parameters(size_message) == False


def test_msg_is_size(size_message,parameters_message):
    assert msg_is_size(parameters_message) == False
    assert msg_is_size(size_message) == True


from sklearn.utils.estimator_checks import check_estimator

def test_prediction_from_rf(window,parameters_message,rf_model):
    assert check_estimator(rf_model)
    assert len(parameters_message['params']) == 4
         
    n_tot,w = prediction_from_rf(window, parameters_message, rf_model)
    assert type(n_tot)==int
    assert type(w)==float




@pytest.fixture
def predictions_history_fixture():
    '''Returns a predictions_history instance '''
    return {(600,'cid1'):'unimportant_value_1', (600,'cid2'):'unimportant_value_2',(1200,'cid5'):'unimportant_value_5'}

@pytest.fixture
def waiting_for_prediction_fixture():
    '''Returns a waiting_for_prediction instance '''
    return {(600,'cid3'):'unimportant_value_3', (600,'cid4'):'unimportant_value_4', (1200,'cid5'):'unimportant_value_6'}


def test_was_waiting_for_prediction(waiting_for_prediction_fixture): 
    waiting_for_prediction = waiting_for_prediction_fixture
    assert was_waiting_for_prediction(600, 'cid1') == False
    assert was_waiting_for_prediction(600, 'cid2') == False
    assert was_waiting_for_prediction(600, 'cid3') == True
    assert was_waiting_for_prediction(600, 'cid4') == True
    
    assert was_waiting_for_prediction(800, 'cid3') == False



def test_find_waiting_size_message(waiting_for_prediction_fixture):
    waiting_for_prediction = waiting_for_prediction_fixture
    assert find_waiting_size_message(600, 'cid3') == 'unimportant_value_3'
    assert find_waiting_size_message(600, 'cid4') == 'unimportant_value_4'
    


def test_already_predicted(predictions_history_fixture):  
    predictions_history = predictions_history_fixture
    assert already_predicted(600, 'cid1') == True
    assert already_predicted(600, 'cid2') == True
    assert already_predicted(600, 'cid3') == False
    assert already_predicted(600, 'cid4') == False
    
    assert was_waiting_for_prediction(800, 'cid3') == False

    

def test_delete_size_and_prediction(waiting_for_prediction_fixture, predictions_history_fixture):
    waiting_for_prediction = waiting_for_prediction_fixture
    predictions_history = predictions_history_fixture
    delete_size_and_prediction(1200,'cid5')
    assert already_predicted(1200,'cid5')==False
    assert was_waiting_for_prediction(1200,'cid5')==False
