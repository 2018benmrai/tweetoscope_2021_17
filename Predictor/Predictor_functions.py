def msg_is_parameters(msg_value):
    return(msg_value['type']=='parameters')

def msg_is_size(msg_value):
    return(msg_value['type']=='size')
    
    
    
def prediction_from_rf(window, parameters):
    p, beta, n_etoile, G = parameters['params']
    w = models[window].predict(beta,n_etoile,G)
    
    n_tot = parameters['n_obs'] + w*G/(1-n_etoile)
    
    return(n_tot,w)
    
    
def save_prediction_in_memory(window, parameters, n_tot):
    predictions[(window,parameters['cid'])]={'n_tot':n_tot, 'w':w, 'beta':beta, 'n_etoile':n_etoile, 'G':G}
    
    
    
def write_on_alerts_topic(window, parameters, n_tot):
    msg = {
        'type':'alert',
        'cid':parameters['cid'],
        'msg':parameters['msg'],
        'T_obs':window,
        'n_tot':n_tot
    }
    
    producer.send(PredictorParams.out_alerts, '', value=msg)
    
    
    
def find_previous_prediction(window, cid):
    return(prediction[(window,cid)])
    
    
    
def write_on_stats_topic(window, message_size, previous_prediction):
    n_tot=previous_prediction['n_tot']
    n_true=message_size['n_tot']
    ARE = abs(n_tot-n_true)/n_true
    
    msg = {
        'type':'stat',
        'cid':message_size['cid'],
        'T_obs':window,
        'ARE':ARE
    }
    
    producer.send(PredictorParams.out_stats, key='', value=msg)
    
    
    
def write_on_samples_topic(window=window, message_size=msg_value, previous_prediction=previous_prediction)
    msg = {
        'type':'sample',
        'cid':message_size['cid'],
        'X':[previous_prediction['beta'],previous_prediction['n_etoile'],previous_prediction['G']],
        'W':previous_prediction['w']
    }
    
    producer.send(PredictorParams.out_samples, key=window, value=msg)



import pickle
def update_model(window=msg.key, pickle_model = msg.value):
    models[window]=pickle.loads(msg.value)
