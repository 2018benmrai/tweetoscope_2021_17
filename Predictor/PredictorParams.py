observation = []

with open ("predictor.ini",'r') as f :
    for line in f :
        line.strip()
        if line[0] == '#' or line[0] == '[' :
            None
        else :
            params = line.strip().split('=')
            if params [0] == "brokers" : 
                continue
            if params [0] == "in_1" : 
                topicIn_cascade_properties = params [1]
            if params [0] == "in_2" : 
                topicIn_models = params [1]
            if params [0] == "out_1" : 
                out_samples = params [1]
            if params [0] == "out_2" : 
                out_alerts = params [1]
            if params [0] == "out_3" : 
                out_stats = params [1]
            if params [0] == "observation" : 
                observation.append(params [1])

with open ("local.ini", 'r') as f :
    for line in f :
        line.strip()
        if line[0] == '#' or line[0] == '[' :
            None
        else :
            params = line.strip().split('=')
            if params [0] == "run_local" : 
                if params[1]=="True":
                    brokers = "localhost:9092"
                else:
                    brokers = "kafka-service:9092"

           
      

#Test récupération paramètres
print (brokers)
print (topicIn_cascade_properties)
print (topicIn_models)
print (out_samples)
print (out_alerts)
print (out_stats)
print (observation)
