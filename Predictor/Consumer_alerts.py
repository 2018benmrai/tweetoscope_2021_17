from kafka import KafkaConsumer
import PredictorParams
import json

consumer_out_alerts = KafkaConsumer(PredictorParams.out_alerts,
                                bootstrap_servers = PredictorParams.brokers,
                                value_deserializer=lambda v: json.loads(v.decode('utf-8')),
                                key_deserializer= lambda v: v.decode())
for msg in consumer_out_alerts:
    print(f"msg: ({msg.key}, {msg.value})")